package com.kenfogel.gopigo3controllerjsf;

import com.kenfogel.gopigo3driver.GoPiGo3;
import com.kenfogel.gopigo3driver.GoPiGo3Servo;

import java.io.Serializable;
import javax.inject.Named;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

/**
 * Backing bean for index.xhtml This class sends messages to the GoPiGo3
 * controller class
 *
 * @author Ken Fogel
 * @version 0.1
 */
@Named("gopigoBacking")
@SessionScoped
public class GoPiGoBacking implements Serializable {

    private static final byte MOTOR_LEFT = 1;
    private static final byte MOTOR_RIGHT = 2;

    static final Logger LOG = Logger.getLogger(GoPiGoBacking.class.getName());

    @Inject
    private GoPiGo3 gopigo;

    @Inject
    private GoPiGo3Servo gopigo3Servo;

    private int distance;

    /**
     * Constructor initializes GoPiGo3 and UltrasonicSensor
     *
     * @throws Exception
     */
    public GoPiGoBacking() throws Exception {
        super();
    }

    /**
     * Forward direction
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goForward() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving forward");
        gopigo.runMotor(MOTOR_LEFT, (byte) 100);
        gopigo.runMotor(MOTOR_RIGHT, (byte) 100);
    }

    /**
     * Reverse direction
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goReverse() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving reverse");
        gopigo.runMotor(MOTOR_LEFT, (byte) -100);
        gopigo.runMotor(MOTOR_RIGHT, (byte) -100);
    }

    /**
     * Turn Left
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goLeft() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving left");
        gopigo.stopMotor(MOTOR_LEFT);
        gopigo.runMotor(MOTOR_RIGHT, (byte) 100);
    }

    /**
     * Turn Right
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goRight() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving right");
        gopigo.stopMotor(MOTOR_RIGHT);
        gopigo.runMotor(MOTOR_LEFT, (byte) 100);
    }

    /**
     * Stop the motors
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goStop() throws IOException, InterruptedException {
        gopigo.stopMotors();
    }

    /**
     * Turn on both LED eyes
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goTurnOnBothLED() throws IOException, InterruptedException {
        gopigo.doLeftEyeLED((byte) 100, (byte) 100, (byte) 100);
        gopigo.doRightEyeLED((byte) 100, (byte) 100, (byte) 100);
    }

    /**
     * Turn off both LED eyes
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goTurnOffBothLED() throws IOException, InterruptedException {
        gopigo.doLeftEyeLED((byte) 0, (byte) 0, (byte) 0);
        gopigo.doRightEyeLED((byte) 0, (byte) 0, (byte) 0);
    }

    public void goReadUltrasonicSensor() throws IOException, InterruptedException {
        distance = gopigo.doUltra();
    }

    /**
     * Retrieve the value from the Ultrasonic Sensor
     *
     * @return
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    public int getUltraValue() throws IOException, InterruptedException {
        return distance;
    }

    /**
     * Start the servo
     *
     */
    public void startServo() {
        LOG.log(Level.INFO, "Start Servo");
        try {
            gopigo3Servo.set_servo((byte) 1);
        } catch (IOException | InterruptedException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Stop the servo
     */
    public void stopServo() {
        try {
            gopigo3Servo.stop_servo((byte)1);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

}
